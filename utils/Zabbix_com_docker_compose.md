# Zabbix com docker compose

Autor: Matheus Freitas Santos

E-mail: mtfrsantos@gmail.com

Repositório: [https://gitlab.com/mtfrsantos/zabbix-docker-compose/](https://gitlab.com/mtfrsantos/zabbix-docker-compose/)

### Pré-requisitos:

- Docker: [https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)
- Docker Compose: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)
- Opcionais Importantes: [https://docs.docker.com/engine/install/linux-postinstall/](https://docs.docker.com/engine/install/linux-postinstall/)

Apesar de existir arquivos prontos do zabbix com o docker-compose ([https://github.com/zabbix/zabbix-docker](https://github.com/zabbix/zabbix-docker)). Os arquivos são longos e cheio de funcionalidades que dependendo do propósito se tornam desnecessários. O objetivo deste documento é oferecer a alternativa mínima funcional do zabbix rodando em contêineres docker.

---

- Crie uma pasta onde vai ficar o arquivo do docker compose e onde ficará armazenado os dados do zabbix localmente, ou seja, fora dos contêineres zabbix;

Ressalta-se a importância de manter os dados do banco de dados salvos na máquina local, pois todo o restante é facilmente recuperável.

```bash
$ mkdir zabbix
$ cd zabbix
```

- Faça a criação de uma rede no docker;

```bash
$ docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zbx-net
```

- Abra o editor de texto/IDE de sua preferência e crie um arquivo chamado docker-compose.yaml;

Arquivos da extensão YAML são sensíveis á indentação.

- Dentro desse arquivo coloque na primeira linha a versão do formato de arquivo do docker compose de sua preferência ([https://docs.docker.com/compose/compose-file/](https://docs.docker.com/compose/compose-file/));

```yaml
version: '3.5'
```

- Na próxima linha iremos começar os nossos serviços do docker compose, ou seja, os contêineres que irão rodar simultaneamente;

```yaml
version: '3.5'
services:
```

- Agora nomearemos os nossos serviços e colocaremos o respectivo nome ao contêiner e a imagem a partir do qual será criado esse contêiner. Começando pelo banco de dados;

Existe vários tipos de bancos de dados diferentes suportados pelo zabbix, escolha um de sua preferência, consulte [https://github.com/zabbix/zabbix-docker](https://github.com/zabbix/zabbix-docker) para mais informações.

```yaml
version: '3.5'
services:

	zbx-db:
		container_name: zbx-db
    image: postgres:latest
```

- O próximo passo é a associação de pastas da máquina local para a respectiva pasta dentro do contêiner, ou seja, tanto a sua máquina, quanto o contêiner terão acesso aos mesmo diretórios;

Caso associe diretórios que não existam, o docker compose irá criá-los.

O modo pode ser :ro para o modo somente leitura e :rw para modo leitura e escrita.

```yaml
version: '3.5'
services:

	zbx-db:
		container_name: zbx-db
    image: postgres:latest
		volumes:
      - ./zbx-server/postgres/data:/var/lib/postgresql:rw
```

- Para a configuração dos contêineres zabbix utiliza-se o ambiente environment do docker compose para alterar as variáveis;

As variáveis são diferentes para cada tipo de banco de dados, [https://github.com/zabbix/zabbix-docker](https://github.com/zabbix/zabbix-docker) para mais informações.

```yaml
version: '3.5'
services:

	zbx-db:
		container_name: zbx-db
    image: postgres:latest
		volumes:
      - ./zbx-server/postgres/data:/var/lib/postgresql:rw
		environment:
      - POSTGRES_USER=zabbix
      - POSTGRES_PASSWORD=zabbix_pwd
      - POSTGRES_DB=zabbix
```

- A configuração de portas permite associar uma porta da máquina local a uma porta dentro do contêiner. Lembrando que o modo padrão de criação de um contêiner é o modo bridge em que o contêiner ganha um endereço IP próprio;

A porta utilizada pelo contêiner varia dependendo do tipo de banco de dados utilizado.

```yaml
version: '3.5'
services:

	zbx-db:
		container_name: zbx-db
    image: postgres:latest
		volumes:
      - ./zbx-server/postgres/data:/var/lib/postgresql:rw
		environment:
      - POSTGRES_USER=zabbix
      - POSTGRES_PASSWORD=zabbix_pwd
      - POSTGRES_DB=zabbix
		ports:
      - "5432:5432"
```

- Configurações adicionais incluem:

```yaml
version: '3.5'
services:

	zbx-db:
		container_name: zbx-db
    image: postgres:latest
		volumes:
      - ./zbx-server/postgres/data:/var/lib/postgresql:rw
		environment:
      - POSTGRES_USER=zabbix
      - POSTGRES_PASSWORD=zabbix_pwd
      - POSTGRES_DB=zabbix
		ports:
      - "5432:5432"
    user: root
    restart: always
```

O que permite a utilização do usuário root dentro do contêiner (que não é o mesmo usuário root da máquina) e o contêiner ser reiniciado caso tenha algum problema.

Isso conclui a configuração do primeiro contêiner do zabbix que é o contêiner de banco de dados.

---

- O próximo contêiner é o servidor do zabbix que inclui configurações a mais.

```yaml
version: '3.5'
services:

	zbx-db:
		container_name: zbx-db
    image: postgres:latest
		volumes:
      - ./zbx-server/postgres/data:/var/lib/postgresql:rw
		environment:
      - POSTGRES_USER=zabbix
      - POSTGRES_PASSWORD=zabbix_pwd
      - POSTGRES_DB=zabbix
		ports:
      - "5432:5432"
    user: root
    restart: always

	zbx-server:
    container_name: zbx-server
    image: zabbix/zabbix-server-pgsql:alpine-5.2-latest
    volumes:
      - ./zbx-server/alertscripts:/usr/lib/zabbix/alertscripts:ro
      - ./zbx-server/externalscripts:/usr/lib/zabbix/externalscripts:ro
      - ./zbx-server/modules:/var/lib/zabbix/modules:ro
      - ./zbx-server/enc:/var/lib/zabbix/enc:ro
      - ./zbx-server/ssh_keys:/var/lib/zabbix/ssh_keys:ro
      - ./zbx-server/mibs:/var/lib/zabbix/mibs:ro
      - ./zbx-server/snmptraps:/var/lib/zabbix/snmptraps:ro
    environment:
      - DB_SERVER_HOST=zbx-db
      - POSTGRES_USER=zabbix
      - POSTGRES_PASSWORD=zabbix_pwd
      - POSTGRES_DB=zabbix
    restart: always
    links:
      - zbx-db
    ports:
      - "10051:10051"
    depends_on:
      - zbx-db
```

O zabbix server está disponível em contêineres de diferentes sistemas operacionais.

A variável DB_SERVER_HOST deve ser igual ao nome do contêiner onde está localizado o banco de dados.

As variáveis POSTGRES_USER, POSTGRES_PASSWORD e POSTGRS_DB devem ser iguais a definidas no campo environment do banco de dados.

Podemos observar dois novos ambientes: links e depends_on. O primeiro faz a ligação entre contêineres e o segundo estabelece uma prioridade, onde a execução do servidor zabbix vai depender da execução do contêiner que contém o banco de dados.

- O *frontend* do zabbix é criado a partir do seguinte contêiner:

```yaml
version: '3.5'
services:

	zbx-db:
		container_name: zbx-db
    image: postgres:latest
		volumes:
      - ./zbx-server/postgres/data:/var/lib/postgresql:rw
		environment:
      - POSTGRES_USER=zabbix
      - POSTGRES_PASSWORD=zabbix_pwd
      - POSTGRES_DB=zabbix
		ports:
      - "5432:5432"
    user: root
    restart: always

	zbx-server:
    container_name: zbx-server
    image: zabbix/zabbix-server-pgsql:alpine-5.2-latest
    volumes:
      - ./zbx-server/alertscripts:/usr/lib/zabbix/alertscripts:ro
      - ./zbx-server/externalscripts:/usr/lib/zabbix/externalscripts:ro
      - ./zbx-server/modules:/var/lib/zabbix/modules:ro
      - ./zbx-server/enc:/var/lib/zabbix/enc:ro
      - ./zbx-server/ssh_keys:/var/lib/zabbix/ssh_keys:ro
      - ./zbx-server/mibs:/var/lib/zabbix/mibs:ro
      - ./zbx-server/snmptraps:/var/lib/zabbix/snmptraps:ro
    environment:
      - DB_SERVER_HOST=zbx-db
      - POSTGRES_USER=zabbix
      - POSTGRES_PASSWORD=zabbix_pwd
      - POSTGRES_DB=zabbix
    restart: always
    links:
      - zbx-db
    ports:
      - "10051:10051"
    depends_on:
      - zbx-db

	zbx-front:
    container_name: zbx-front
    image: zabbix/zabbix-web-apache-pgsql:alpine-5.2-latest
    environment:
      - ZBX_SERVER_HOST=zbx-server
      - DB_SERVER_HOST=zbx-db
      - POSTGRES_USER=zabbix
      - POSTGRES_PASSWORD=zabbix_pwd
      - POSTGRES_DB=zabbix
    restart: always
    links:
      - zbx-db
      - zbx-server
    ports:
      - "80:8080"
      - "443:8443"
    depends_on:
      - zbx-db
      - zbx-server
```

O *frontend* do zabbix também está disponível em nginx e em contêineres de diferentes sistemas operacionais.

---

O arquivo docker-compose.yaml ainda não está finalizado. A finalização do arquivo vai depender dessas três situações possíveis:

1. Não necessito do coletar dados da própria máquina onde está instalado o zabbix server;
2. Necessito coletar dados da própria máquina onde está instalado o zabbix server, mas não necessito coletar dados dos contêineres docker;
3. Necessito coletar dados da própria máquina onde está instalado o zabbix server e dos contêineres docker.

- Situação 1:

Para finalizar o arquivo vai precisar somente configurar o acesso dos contêineres a rede do docker anteriormente criada zbx-net.

```yaml
version: '3.5'
services:

	zbx-db:
		container_name: zbx-db
    image: postgres:latest
		volumes:
      - ./zbx-server/postgres/data:/var/lib/postgresql:rw
		environment:
      - POSTGRES_USER=zabbix
      - POSTGRES_PASSWORD=zabbix_pwd
      - POSTGRES_DB=zabbix
		ports:
      - "5432:5432"
    user: root
    restart: always

	zbx-server:
    container_name: zbx-server
    image: zabbix/zabbix-server-pgsql:alpine-5.2-latest
    volumes:
      - ./zbx-server/alertscripts:/usr/lib/zabbix/alertscripts:ro
      - ./zbx-server/externalscripts:/usr/lib/zabbix/externalscripts:ro
      - ./zbx-server/modules:/var/lib/zabbix/modules:ro
      - ./zbx-server/enc:/var/lib/zabbix/enc:ro
      - ./zbx-server/ssh_keys:/var/lib/zabbix/ssh_keys:ro
      - ./zbx-server/mibs:/var/lib/zabbix/mibs:ro
      - ./zbx-server/snmptraps:/var/lib/zabbix/snmptraps:ro
    environment:
      - DB_SERVER_HOST=zbx-db
      - POSTGRES_USER=zabbix
      - POSTGRES_PASSWORD=zabbix_pwd
      - POSTGRES_DB=zabbix
    restart: always
    links:
      - zbx-db
    ports:
      - "10051:10051"
    depends_on:
      - zbx-db

	zbx-front:
    container_name: zbx-front
    image: zabbix/zabbix-web-apache-pgsql:alpine-5.2-latest
    environment:
      - ZBX_SERVER_HOST=zbx-server
      - DB_SERVER_HOST=zbx-db
      - POSTGRES_USER=zabbix
      - POSTGRES_PASSWORD=zabbix_pwd
      - POSTGRES_DB=zabbix
    restart: always
    links:
      - zbx-db
      - zbx-server
    ports:
      - "80:8080"
      - "443:8443"
    depends_on:
      - zbx-db
      - zbx-server

networks:
  default:
    external:
      name: zbx-net
```

O parâmetro default faz com que essa rede seja adotada por todos os contêineres nos quais não estão configurados nenhuma rede. O comando external permite o acesso a uma rede externa previamente criada.

O arquivo docker-compose.yml está finalizado para a Situação 1. Restando apenas executar o comando abaixo para colocar de pé todos os contêineres.

```bash
$ docker-compose up -d
```

Você deve estar no mesmo diretório que contém o arquivo docker-compose.yaml para executar o comando acima.

- Situação 2:

Nessa Situação o arquivo docker-compose.yaml é finalizado da mesma maneira da Situação 1, mas logo após a execução do comando `docker-compose up -d` há a necessidade da criação de um contêiner adicional com agente de coleta de dados do zabbix. O que é facilmente criado pelo seguinte comando:

```bash
$ docker run --name zbx-agent --privileged -p 10050:10050 --network=zbx-net -e ZBX_HOSTNAME="MyHost" -e ZBX_SERVER_HOST=<IP_ZABBIX_SERVER> -v /dev/sdc:/dev/sdc:ro -d zabbix/zabbix-agent:alpine-5.2-latest
```

⚠️ Fatos para se atentar sobre o zabbix *agent*:

- A variável ZBX_HOSTNAME guarda o nome da máquina do qual se deseja monitorar e deve ser o mesmo nome utilizado quando for adicionar esse *host* no *frontend* do zabbix. Caso contrário, não será possível coletar dados de forma ativa;
- Na variável ZBX_SERVER_HOST é onde colocamos o endereço IP do contêiner em que está localizado o servidor zabbix. Para localizar essa informação devemos utilizar o comando abaixo e demos procurar nas últimas linhas e olhar a linha que contém "IPAddress";

    ```bash
    $ docker inspect zbx-server
    ```

- De forma semelhante, para adicionar esse *host* no *frontend* do zabbix devemos também localizar o endereço IP do zabbix *agent*.

    ```bash
    $ docker inspect zbx-agent
    ```

    - Situação 3:

    Nessa Situação podemos colocar o zabbix *agent* juntamente com os outros contêineres dentro do docker-compose.yaml. Atente-se ao fato de que na Situação 2 utiliza-se a imagem do agente zabbix/zabbix-agent:alpine-5.2-latest, enquanto nessa situação utiliza-se a zabbix/zabbix-agent2:alpine-5.2-latest que é a imagem mais adequada para monitorar os contêineres do docker.

    ```yaml
    version: '3.5'
    services:

    	zbx-db:
    		container_name: zbx-db
        image: postgres:latest
    		volumes:
          - ./zbx-server/postgres/data:/var/lib/postgresql:rw
    		environment:
          - POSTGRES_USER=zabbix
          - POSTGRES_PASSWORD=zabbix_pwd
          - POSTGRES_DB=zabbix
    		ports:
          - "5432:5432"
        user: root
        restart: always

    	zbx-server:
        container_name: zbx-server
        image: zabbix/zabbix-server-pgsql:alpine-5.2-latest
        volumes:
          - ./zbx-server/alertscripts:/usr/lib/zabbix/alertscripts:ro
          - ./zbx-server/externalscripts:/usr/lib/zabbix/externalscripts:ro
          - ./zbx-server/modules:/var/lib/zabbix/modules:ro
          - ./zbx-server/enc:/var/lib/zabbix/enc:ro
          - ./zbx-server/ssh_keys:/var/lib/zabbix/ssh_keys:ro
          - ./zbx-server/mibs:/var/lib/zabbix/mibs:ro
          - ./zbx-server/snmptraps:/var/lib/zabbix/snmptraps:ro
        environment:
          - DB_SERVER_HOST=zbx-db
          - POSTGRES_USER=zabbix
          - POSTGRES_PASSWORD=zabbix_pwd
          - POSTGRES_DB=zabbix
        restart: always
        links:
          - zbx-db
        ports:
          - "10051:10051"
        depends_on:
          - zbx-db

    	zbx-front:
        container_name: zbx-front
        image: zabbix/zabbix-web-apache-pgsql:alpine-5.2-latest
        environment:
          - ZBX_SERVER_HOST=zbx-server
          - DB_SERVER_HOST=zbx-db
          - POSTGRES_USER=zabbix
          - POSTGRES_PASSWORD=zabbix_pwd
          - POSTGRES_DB=zabbix
        restart: always
        links:
          - zbx-db
          - zbx-server
        ports:
          - "80:8080"
          - "443:8443"
        depends_on:
          - zbx-db
          - zbx-server

    	zbx-agent:
        container_name: zbx-agent
        image: zabbix/zabbix-agent2:alpine-5.2-latest
        volumes:
         - /var/run/docker.sock:/var/run/docker.sock
         - /etc/localtime:/etc/localtime:ro
         - ./zbx_env/etc/zabbix/zabbix_agentd.d:/etc/zabbix/zabbix_agentd.d:ro
         - ./zbx_env/var/lib/zabbix/modules:/var/lib/zabbix/modules:ro
         - ./zbx_env/var/lib/zabbix/enc:/var/lib/zabbix/enc:ro
         - ./zbx_env/var/lib/zabbix/ssh_keys:/var/lib/zabbix/ssh_keys:ro
        environment:
          - ZBX_HOSTNAME=<MeuHost>
          - ZBX_SERVER_HOST=<IP_ZABBIX_SERVER>
        ports:
         - "10050:10050"
        user: root
        privileged: true
        pid: "host"

    networks:
      default:
        external:
          name: zbx-net
    ```

    Deve-se substituir `<MeuHost>` e `<IP_ZABBIX_SERVER>` por um nome para seu *host* e o IP do servidor zabbix, respectivamente.

    Os mesmo fatos para se atentar na Situação 2 valem para essa situação.

    Logo após, execute `docker-compose up -d`  para rodar os contêineres.

    ---

    Comandos úteis:

    - `docker-compose stop` : Para de executar todos os contêineres dentro do docker-compose.yaml;
    - `docker-compose rm` : Exclui os contêineres criados pelo docker-compose.yaml.
